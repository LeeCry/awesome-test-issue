package TestIssue;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;

public class ProductTest extends BaseTest {

    @BeforeMethod
    public void beforeEachTest() throws IOException {
        mainPage.init();
        searchPage.init();
        searchResultPage.init();
        mainPage.openWebsite("https://www.ss.lv/");
        maximize();
    }

    @Test
    public void productTest() {
        mainPage.switchLanguage();
        mainPage.getSection();
        mainPage.clickSearch();

        searchPage.enterSearchPhrase("Computer");
        searchPage.selectTownByIndex(1);
        searchPage.selectPeriodByIndex(2);
        searchPage.clickSearchBtn();

        searchResultPage.sortByPrice();
        searchResultPage.selectSortBy("Продажа");
    }
}
