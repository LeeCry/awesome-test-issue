package TestIssue;

import org.openqa.selenium.WebDriver;

import java.io.IOException;


public class MainPagePO extends BasePO {
    public WebDriver driver;

    public MainPagePO(WebDriver driver) {
        this.driver = driver;
    }

    public void openWebsite (String url) throws IOException {
        driver.get(url);
    }

    public void switchLanguage() {
        driver.findElement(parser.getObjectLocator("LanguageSwitcher")).click();
    }

    public void getSection() {
        driver.findElement(parser.getObjectLocator("ElectroTechnics")).click();
    }

    public void clickSearch() {
        driver.findElement(parser.getObjectLocator("SearchBTN")).click();
    }
}
