package TestIssue;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;


public class SearchPO extends BasePO {
    public WebDriver driver;


    public SearchPO(WebDriver driver) {
        this.driver = driver;
    }

    public void enterSearchPhrase(String phrase) {
        driver.findElement(parser.getObjectLocator("SearchPhraseInput")).sendKeys(phrase);
    }

    public void selectTownByIndex(int index) {
        Select townDropdown = new Select(driver.findElement(parser.getObjectLocator("TownSelector")));
        townDropdown.selectByIndex(index);
    }

    public void selectPeriodByIndex(int index) {
        Select periodDropdown = new Select(driver.findElement(parser.getObjectLocator("PeriodSelector")));
        periodDropdown.selectByIndex(index);
    }

    public void clickSearchBtn() {
        driver.findElement(parser.getObjectLocator("FindBTN")).click();
    }
}
