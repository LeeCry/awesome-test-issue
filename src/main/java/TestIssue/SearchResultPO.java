package TestIssue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class SearchResultPO extends BasePO {
    public WebDriver driver;

    public SearchResultPO(WebDriver driver) {
        this.driver = driver;
    }

    public void sortByPrice() {
        driver.findElement(parser.getObjectLocator("PriceSorter")).click();
    }

    public void selectSortBy(String sort) {
        Select periodDropdown = new Select(driver.findElement(parser.getObjectLocator("SortSelector")));
        periodDropdown.selectByVisibleText(sort);
    }
}
